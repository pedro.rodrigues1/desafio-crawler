import unittest
from puctuation_cleaner import remove_puct
from tag_cleaner import remove_tags
from stopwords_cleaner import remove_stopwords
from word_count import conta_palavras


class TestCrawler(unittest.TestCase):

    def test_remove_puc(self):
        texto = 'A. C . ! : D'
        teste = remove_puct(texto)
        resultado = 'A C    D'
        self.assertEqual(teste,resultado)

    def test_remove_tag(self):
        texto = '<br>A inteligência artificial<br> dos inimigos também é um dos...</p></div></div></div>'
        teste = remove_tags(texto)
        resultado = 'A inteligência artificial dos inimigos também é um dos...'
        self.assertEqual(teste,resultado)
 
    def test_remove_stopwords(self):
        texto = 'A inteligência artificial dos inimigos também é um de inimigos'
        teste = remove_stopwords(texto)
        resultado = 'inteligência artificial inimigos inimigos'
        self.assertEqual(teste,resultado)

    def test_conta_palavras(self):
        texto = 'inteligência artificial inimigos inimigos'
        teste = str(conta_palavras(texto))
        self.assertTrue(True if 'inimigos' and '2' in teste else False)

    def fluxo(self, entrada):
        try:
            if '!' in entrada:
                if '<h2>' in entrada:
                    if 'de' in entrada:
                        if '-' in entrada:
                            return False
            else:
                return True
        except:
            return False


    def test_integado(self):
        entrada = '<h2>arroz ! feijao <small>--------cebola</small>? macarrao </h2> arroz de da de'
        entrada = str(conta_palavras(remove_stopwords(remove_puct(remove_tags(entrada)))))
        saida = self.fluxo(entrada)
        self.assertTrue(saida)   
     

if __name__ == '__main__':
    unittest.main()
