from bs4 import BeautifulSoup
import sys


def remove_tags(txt):
    soup = BeautifulSoup(txt,'html.parser') # crie um novo objeto bs4 a partir dos dados html carregados
    for script in soup(["script", "style"]): # remove todo o código javascript e stylesheet css
        script.extract()
    
    text = soup.get_text()
    
    return text


if __name__ == "__main__":
    for line in sys.stdin:
        print(remove_tags(line.strip()))
    
    


