import requests
from tag_cleaner import remove_tags
from puctuation_cleaner import remove_puct
from stopwords_cleaner import remove_stopwords
from word_count import conta_palavras
from bs4 import BeautifulSoup
from sys import argv
import re
import hashlib


def captura(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    img = soup.find('img')
    materia = soup.findAll("p")
    return img, str(materia)


def limpa_materia(materia):
    texto_encode = materia
    texto_no_tag = remove_tags((texto_encode))
    texto_no_puct = remove_puct(texto_no_tag)
    texto_no_stopwords = remove_stopwords(texto_no_puct)
    texto = conta_palavras(texto_no_stopwords)
    return texto


def grava_materia(url):
    id = gera_id(url)
    img, texto = captura(url)

    materia = open(f'materias/materia_{id}.html', 'w')
    materia.write(f'<br>{css()}<br><br>{str(img)}<br><br>{str(limpa_materia(texto))}<br><br>')
    materia.close
    
    index = open('index.html', 'a')
    index.write(f'<a href=materias/materia_{id}.html>{url}</a><br><br>')
    index.close


def gera_id(url):
    id = hashlib.md5(url.encode()).hexdigest()
    return id
   

def css():
    css = """
    <style type="text/css"> 
    body{
    
    text-align: center;  
   
    }
    
    img{

    width: 500px;

    height: 350px;

    }
    
    </style> """
    
    return css


if __name__ == "__main__":
    url = argv[1]
    grava_materia(url)
