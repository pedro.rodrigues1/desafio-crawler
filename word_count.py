import sys

def conta_palavras(txt):
    count_result = {}
    texto = list(txt.capitalize().split())
    for result in texto:
        count_result[result] = 1 if count_result.get(result) is None else count_result[result] + 1
    
    sorted_x = sorted(count_result.items(), key=lambda x: x[1])
    dezmais = []
    i=-1
    if len(sorted_x) > 9:
        while i != -11:
            dezmais.append((sorted_x[i]))
            i -=1
    else:
        dezmais.append(sorted_x)
    return  dezmais



if __name__ == "__main__":
    for line in sys.stdin:
        print(conta_palavras(line.strip()))
